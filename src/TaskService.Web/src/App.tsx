import React from 'react';
import logo from './logo.svg';
import './App.css';
import TaskList from './components/TaskList';

function App() {

  return (
    <div className="container-fluid">
      <header >

      </header>
      <main className='mt-3'>
        <div className="row">
          <div className="col">
            <TaskList title='Список задач'></TaskList>
          </div>
          <div className="col">

          </div>
          <div className="col">

          </div>
        </div>
      </main>
    </div>
  );
}

export default App;
