interface ITaskListItemProps {
    id: string;
    text: string;
    deleteItem: (id: string) => void
}

const TaskListItem = ({ id, text, deleteItem }: ITaskListItemProps) => {

    return (
        <li className="list-group-item">
        {text}
        <a href="#" className="btn btn-danger float-end" onClick={(e) => {
            e.preventDefault();
            deleteItem(id);
        }}>Удалить</a>
    </li>
    )
}

export default TaskListItem;

