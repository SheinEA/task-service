import React, { Component } from 'react';
import ITask from '../models/ITask';
import TaskService from '../services/TasksService';
import TaskListItem from './LaskListItem';

interface ITaskListProps {
  title: string;
}

type State = {
  tasks: Array<ITask>
};

class TaskList extends Component<ITaskListProps, State> {

  constructor(props: ITaskListProps) {
    super(props);

    this.state = {
      tasks: []
    };
  }

  buttonHandler = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
    TaskService.getAll()
      .then(r => {
        this.setState({
          tasks: r.data
        });
      })
  };

  onDeleteItem = (id: string): void => {
    TaskService.delete(id)
      .then(r => {
        if (r.status === 200) {
          const tasks = this.state.tasks.filter(c => c.id !== id);
          this.setState({
            tasks: tasks
          });
        }

      })
  };

  render() {
    const { title, children } = this.props;
    return (
      <>
        <button className='btn btn-primary' onClick={this.buttonHandler}>
          Получить задачи
        </button>
        <div className="card mt-1">
          <div className="card-header">
            {title}
          </div>
          <ul className="list-group list-group-flush">
            {this.state.tasks.map(t => <TaskListItem key={t.id} id={t.id} text={t.name} deleteItem={this.onDeleteItem} />)}
          </ul>
        </div>
      </>
    );
  }
}

export default TaskList;