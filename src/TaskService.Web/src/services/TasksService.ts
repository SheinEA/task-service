import http from "./http-base";
import ITask from '../models/ITask';

class TasksService {
  getAll() {
    return http.get<Array<ITask>>('/Tasks');
  }

  get(id: string) {
    return http.get<ITask>(`/Tasks/${id}`);
  }

  delete(id: string) {
    return http.delete<string>(`/Tasks/${id}`);
  }
}

export default new TasksService();