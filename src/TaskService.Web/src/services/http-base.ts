import axios from 'axios';

export default axios.create({
  baseURL: (process.env.REACT_APP_API_URL as string),
  headers: {
    'Content-type': 'application/json'
  }
});