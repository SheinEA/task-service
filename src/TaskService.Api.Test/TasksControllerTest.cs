using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using TaskService.Data;

namespace TaskService.Api.Test
{
    public class TasksControllerTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Create_OnlyName_Ok()
        {
            var mockRepo = new Mock<ITaskRepository>();
            var mockLogger = new Mock<ILogger<TasksController>>();
            var controller = new TasksController(mockLogger.Object, mockRepo.Object);

            var result = controller.Create("Task 1", "", "");

            Assert.True(result is OkResult);
        }

        [Test]
        public void Create_OnlyAuthor_BadRequest()
        {
            var mockRepo = new Mock<ITaskRepository>();
            var mockLogger = new Mock<ILogger<TasksController>>();
            var controller = new TasksController(mockLogger.Object, mockRepo.Object);

            var result = controller.Create("", "Shein", "");

            Assert.True(result is BadRequestResult);
        }
    }
}