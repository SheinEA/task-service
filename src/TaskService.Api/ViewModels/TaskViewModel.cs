namespace TaskService.Api;

public class TaskViewModel
{
    public string? Id {get ; set;}

    public string? AuthorId { get; set; }

    public string? Name { get; set; }

    public string? Description { get; set; }

    public DateTime CreatedDate { get; set; }

    public DateTime ChangedDate { get; set; }
}