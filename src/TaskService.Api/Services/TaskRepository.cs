using System.Diagnostics.CodeAnalysis;
using Task = TaskService.Domain.Task;

namespace TaskService.Data;

public class TaskRepository : ITaskRepository
{
    private readonly TaskContext _context;

    public TaskRepository([NotNullAttribute] TaskContext context)
    {
        _context = context;
    }

    public void Create(Task task)
    {
        _context.Tasks.Add(task);
        _context.SaveChanges();
    }

    public void Delete(string id)
    {
        var entity = _context.Tasks.Find(id);
        if (entity != null)
        {
            _context.Tasks.Remove(entity);
            _context.SaveChanges();
        }

    }

    public Task? Get(string id)
    {
        return _context.Tasks.Find(id);
    }

    public IEnumerable<Task> Get()
    {
        return _context.Tasks.ToList();
    }
}