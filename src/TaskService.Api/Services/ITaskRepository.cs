using Task = TaskService.Domain.Task;

namespace TaskService.Data;

public interface ITaskRepository {

    Task? Get (string id);

    IEnumerable<Task> Get();

    void Create(Task task);

    void Delete (string id);
}