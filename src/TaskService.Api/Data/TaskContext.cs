using Microsoft.EntityFrameworkCore;

namespace TaskService.Data;

public class TaskContext : DbContext
{
    public DbSet<TaskService.Domain.Task> Tasks => Set<TaskService.Domain.Task>();

    public TaskContext(DbContextOptions<TaskContext> options) : base(options)
    {

    }
}