namespace TaskService.Domain;

public class Task
{
    private Task() : this(string.Empty, string.Empty)  { }

    public Task(string name, string authorId) {
        Name = name;
        AuthorId = authorId;
    }

    public string Id { get ; set; } = Guid.NewGuid().ToString();

    public string AuthorId { get; set; } = String.Empty;

    public string Name { get; set; } = String.Empty;

    public string? Description { get; set; }

    public DateTime CreatedDate { get; set; }

    public DateTime ChangedDate { get; set; }
}