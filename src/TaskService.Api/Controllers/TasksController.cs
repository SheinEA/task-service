using Microsoft.AspNetCore.Mvc;
using TaskService.Data;

namespace TaskService.Api;

[ApiController]
[Route("[controller]")]
public class TasksController : ControllerBase
{
    private readonly ILogger<TasksController> _logger;
    ITaskRepository _repository;

    public TasksController(ILogger<TasksController> logger, ITaskRepository repository)
    {
        _logger = logger;
        _repository = repository;
    }

    [HttpGet("{id}")]
    public ActionResult<TaskViewModel> Get(string id)
    {
        var task = _repository.Get(id);

        if (task != null)
        {
            return new TaskViewModel
            {
                Id = task.Id,
                Name = task.Name,
                AuthorId = task.AuthorId,
                ChangedDate = task.ChangedDate,
                CreatedDate = task.CreatedDate,
                Description = task.Description
            };
        }

        return NotFound();
    }

    [HttpGet]
    public ActionResult<IEnumerable<TaskViewModel>> Get()
    {
        var tasks = _repository.Get();
        var taskDescriptionList = new List<TaskViewModel>();

        foreach (var task in tasks)
        {
            taskDescriptionList.Add(
                new TaskViewModel
                {
                    Id = task.Id,
                    Name = task.Name,
                    AuthorId = task.AuthorId,
                    ChangedDate = task.ChangedDate,
                    CreatedDate = task.CreatedDate,
                    Description = task.Description
                }
            );
        }

        return taskDescriptionList;
    }

    [HttpPost]
    public ActionResult Create(string name, string authorId, string description)
    {
        var createdDate = DateTime.Now;

        if(string.IsNullOrWhiteSpace(name)){
            return BadRequest();
        }

        _repository.Create(new TaskService.Domain.Task(name, authorId)
        {
            ChangedDate = createdDate,
            CreatedDate = createdDate,
            Description = description
        });

        return Ok();
    }

    [HttpDelete("{id}")]
    public ActionResult Delete(string id)
    {
        _repository.Delete(id);
        return Ok();
    }
}