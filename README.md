# Task Service
This repository contains the Task Service API project. <br/>
Task Service is a REST API for managing (create, update, delete) user tasks. <br/>
Task Service is a microservice that takes a per-service database approach. Where PostgreSQL is used as the database.

# Tech stack
- NET 6.0
- PostgreSQL 14.1
- Docker

# Repository structure
Here is a hierarchy of directories and files.

```
task-service
├── .gitignore
├── CHANGELOG.md
├── README.md
├── deploy/
│   ├── ansible/
│   │   ├── templates/
│   │   │   └── site.conf.j2
│   │   ├── inventory.yml
│   │   ├── nginx.yml
│   │   └── sync.yml
├── docker/
│   ├── api/
│   |   ├── docker-compose.yml
│   │   └── Dockerfile
│   ├── web/
│   │   └── Dockerfile
│   └── .dockerignore
└── src/
    ├── TaskService.Api/
    │   ├── Controllers/
    │   ├── Models/
    │   ├── TaskService.Api.csproj
    │   ├── appsettings.Development.json
    │   └── appsettings.json
    └── TaskService.sln

```

# Development
Clone this repository.
    
```    
git clone git@gitlab.com:SheinEA/task-service.git 
```

## Run debug api
```
сd task-service\src\TaskService.Api
dotnet run
```

## Run debug web
```
сd task-service\src\TaskService.Web
npm run start
```

Open browser at https://localhost:7173/swagger/index.html

# Build
Clone this repository.
    
```    
git clone git@gitlab.com:SheinEA/task-service.git 
```

## Docker
```
сd task-service
```

### Build docker image web
```
docker build -f deploy/docker/web/Dockerfile -t web-todo:prod .
```

### Build docker image api
```
docker build -f deploy/docker/api/Dockerfile -t api-todo:prod .
```

### Run backend with docker compose
```
docker-compose -f deploy/docker/api/docker-compose.yml -p "todo"  up -d
```